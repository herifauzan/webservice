import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import '../public/css/style.css';
import Login from './View/Login';
import Register from './View/Register';
import Dashboard from './View/Dashboard';
import {AuthContextProvider} from './context/AuthContext';
import ProtectedRoute from './components/ProtectedRoute';

class App extends Component {
  render() {
    return (
      <AuthContextProvider>
        <div className="container">
          <Router>     
          <Switch>
            <ProtectedRoute path="/" exact component={Dashboard} />
            <Route path="/login" component={Login} />
            <Route path="/register" component={Register} />
          </Switch>
          </Router>   
        </div>
      </AuthContextProvider> 
    );
  }
}

export default App;
