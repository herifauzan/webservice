import React, { Component } from 'react';

class Register extends Component {
  constructor(props){
      super(props);
      this.state={
        username:'',
        password:''
      }
  }
  handleClick(event){
    var apiBaseUrl = "http://localhost:3001/api/login";
    var self = this;
    var payload={
      "username":this.state.username,
      "password":this.state.password
    }
  }
  render() {
    return (
      <React.Fragment>
        <form onSubmit={(event) => this.handleClick(event)} method='POST'>
        <h1> Image Forgery Detection Tools</h1>
        <h2>Please Login</h2>
        <hr />
          <label htmlFor="uname">Email or Username</label>
          <input type="text" id="uname" name="username" value={this.state.username} 
          onChange={(event,newValue) => this.setState({username:newValue})}/>

          <label htmlFor="pwd">Password</label>
          <input type="password" id="pwd" name="password" value={this.state.password} 
          onChange={(event,newValue) => this.setState({password:newValue})}/>

          <input type="submit" value="LOGIN" />
          <br />
          <br />
          <br />
          <br />
        </form>
      </React.Fragment>
    );
  }

  requestLogin(params) {
    
    return "";
  }
}

export default Register;
