import React, { Component } from 'react';
import { withAuth } from '../context/AuthContext'
import { Redirect } from "react-router-dom";

class Login extends Component {
  constructor(props){
      super(props);
      this.state={
        username:'',
        password:''
      }
  }
  state = {
    email: "",
    password: ""
  }

  handleChange = (e) => {
      const { name, value } = e.target
      this.setState({
          [name] : value
      })
  }

  handleSubmit = (e) => {
      e.preventDefault();
      this.props.login(this.state)
  }

  render() {
    if(this.props.isLoggedIn) return <Redirect push to='/' />
    return (
        <form onSubmit={this.handleSubmit}>
        <h1> Salestock</h1>
        <h2>Please Login</h2>
        <hr />
        <label htmlFor="uname">Email or Username</label>
        <input onChange={this.handleChange}
                        value={this.state.email}
                        type='text' placeholder='email' name='username' />
        <label htmlFor="pwd">Password</label>
        <input onChange={this.handleChange}
                        value={this.state.password}
                        type='password' placeholder='password' name='password'/>

        <input type='submit' value='LOGIN' />
          <br />
          <br />
          <br />
          <br />
        </form>
    );
  }

  requestLogin(params) {
    
    return "";
  }
}

export default withAuth(Login);
