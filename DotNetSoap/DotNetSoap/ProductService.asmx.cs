﻿using System;
using System.Collections.Generic;
using System.Web.Services;
using DotNetSoap.Model;
using DotNetSoap.App_Code;
using Newtonsoft.Json.Linq;

namespace DotNetSoap
{
    [WebService]
    public class ProductService : WebService
    {
        [WebMethod]
        public List<Product> getAllProduct()
        {
            string result = APIConnect.GetRequestJArray("http://127.0.0.1:5000/api/product");
            JArray arr = JArray.Parse(result);
            List<Product> list = new List<Product>();
            foreach (JObject x in arr.Children<JObject>())
            {
                Product prod = new Product(x);
                list.Add(prod);
            }
            return list;
        }
        [WebMethod]
        public List<Product> getAllProductByName(string keyword)
        {
            string result = APIConnect.GetRequestJArray("http://127.0.0.1:5000/api/values/"+keyword);
            JArray arr = JArray.Parse(result);
            List<Product> list = new List<Product>();
            foreach (JObject x in arr.Children<JObject>())
            {
                Product prod = new Product(x);
                list.Add(prod);
            }
            return list;
        }
        [WebMethod]
        public Product getProduct(int id)
        {
            string result = APIConnect.GetRequestJObject("http://127.0.0.1:5000/api/product/" + id);
            Product product = new Product(JObject.Parse( result));
            return product;
        }
        [WebMethod]
        public string updateProduct(int id)
        {
            
            return "Invoked at : " + DateTime.Now.ToString() + " update product " + id;
        }
        [WebMethod]
        public string createProduct(string name, string description, long price)
        {
            Product prod = new Product(name, description, price);
            string result = "";
            return result;
        }
        [WebMethod]
        public string deleteProduct(int id)
        {
            try
            {
                string result = APIConnect.DeleteRequest("http://127.0.0.1:5000/api/product/" + id);
                if(result != null)
                {
                    return "Delete Successfully";
                }
                else
                {
                    return "ID not found";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}
