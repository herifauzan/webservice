﻿using System;
using Newtonsoft.Json.Linq;

namespace DotNetSoap.Model
{
    [Serializable]
    public class Product
    {
        public int id { get; set; }
        public DateTime create_date { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public long price { get; set; }
        //public string photo { get; set; }
        //public int seller_id { get; set; }
        public Product(JObject productJson)
        {
            id = (int)productJson["id"];
            create_date = (System.DateTime)productJson["create_date"];
            name = (string)productJson["name"];
            description = (string)productJson["description"];
            price = (long)productJson["price"];
            //photo = (string)productJson["photo"];
            //seller_id = (int)productJson["seller_id"];
        }

        public Product()
        {
        }
        public Product(string name, string description, long price)
        {
            this.name = name;
            this.description = description;
            this.price = price;
            this.create_date = DateTime.UtcNow;
        }
    }
}
