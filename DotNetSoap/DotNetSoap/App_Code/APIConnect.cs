﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using DotNetSoap.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace DotNetSoap.App_Code
{
    public class APIConnect
    {
        public static string GetRequestJArray(string url)
        {
            ServicePointManager.ServerCertificateValidationCallback += (p1, p2, p3, p4) => true;
            WebRequest request = WebRequest.Create(url);
            // If required by the server, set the credentials.  
            //request.Credentials = CredentialCache.DefaultCredentials;
            request.ContentType = "application/json";
            string responseFromServer="";
            // Get the response.  
            WebResponse response = request.GetResponse();
            // Display the status.  
            Console.WriteLine(((HttpWebResponse)response).StatusDescription);

            // Get the stream containing content returned by the server. 
            // The using block ensures the stream is automatically closed. 
            using (Stream dataStream = response.GetResponseStream())
            {
                // Open the stream using a StreamReader for easy access.  
                StreamReader reader = new StreamReader(dataStream);
                // Read the content.  
                responseFromServer = reader.ReadToEnd();
                // Display the content.  
                Console.WriteLine(responseFromServer);
            }
            response.Close();
            return responseFromServer;
        }
        public static string GetRequestJObject(string url)
        {
            ServicePointManager.ServerCertificateValidationCallback += (p1, p2, p3, p4) => true;
            WebRequest request = WebRequest.Create(url);
            // If required by the server, set the credentials.  
            //request.Credentials = CredentialCache.DefaultCredentials;
            request.ContentType = "application/json";
            string responseFromServer = "";
            // Get the response.  
            WebResponse response = request.GetResponse();
            // Display the status.  
            Console.WriteLine(((HttpWebResponse)response).StatusDescription);

            // Get the stream containing content returned by the server. 
            // The using block ensures the stream is automatically closed. 
            using (Stream dataStream = response.GetResponseStream())
            {
                // Open the stream using a StreamReader for easy access.  
                StreamReader reader = new StreamReader(dataStream);
                // Read the content.  
                responseFromServer = reader.ReadToEnd();
                // Display the content.  
                Console.WriteLine(responseFromServer);
            }
                        // Close the response.  
            response.Close();
            return responseFromServer;
        }
        public string PostRequestAPI(string query, Product product)
        {
            string json = null;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(query);
            request.Method = "POST";
            request.ContentType = "application/json; charset=utf-8";
            request.KeepAlive = true;
            string x = JsonConvert.SerializeObject(product);
            //request.Headers.Add("Authorization", "Basic " + KEY_REQUEST);
            var sw = new StreamWriter(request.GetRequestStream());
            sw.Write(x);
            sw.Close();

            try
            {
                using (var response = request.GetResponse())
                {
                    using (var stream = new StreamReader(response.GetResponseStream(), Encoding.GetEncoding(1252)))
                    {
                        json = stream.ReadToEnd();
                        // LitInfo.Text += "<br/>[LOG INFO] Retrieving " + json.Children().Count() + " items from GC API";
                    }
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
                //LitInfo.Text += "<br/>[ERROR LOG] " + ExceptionMessage(ex);
            }
            return json;
        }
        public static string PutRequest(string query, Product product)
        {
            string json = null;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(query);
            request.Method = "PUT";
            request.ContentType = "application/json; charset=utf-8";
            request.KeepAlive = true;
            string x = JsonConvert.SerializeObject(product);
            //request.Headers.Add("Authorization", "Basic " + KEY_REQUEST);
            var sw = new StreamWriter(request.GetRequestStream());
            sw.Write(x);
            sw.Close();

            try
            {
                using (var response = request.GetResponse())
                {
                    using (var stream = new StreamReader(response.GetResponseStream(), Encoding.GetEncoding(1252)))
                    {
                        json = stream.ReadToEnd();
                        // LitInfo.Text += "<br/>[LOG INFO] Retrieving " + json.Children().Count() + " items from GC API";
                    }
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
                //LitInfo.Text += "<br/>[ERROR LOG] " + ExceptionMessage(ex);
            }
            return json;
        }
        public static string DeleteRequest(string url)
        {
            ServicePointManager.ServerCertificateValidationCallback += (p1, p2, p3, p4) => true;
            WebRequest request = WebRequest.Create(url);
            request.Method = "Delete";
            // If required by the server, set the credentials.  
            //request.Credentials = CredentialCache.DefaultCredentials;
            request.ContentType = "application/json";
            string responseFromServer = "";
            // Get the response.  
            WebResponse response = request.GetResponse();
            // Display the status.  
            Console.WriteLine(((HttpWebResponse)response).StatusDescription);

            // Get the stream containing content returned by the server. 
            // The using block ensures the stream is automatically closed. 
            using (Stream dataStream = response.GetResponseStream())
            {
                // Open the stream using a StreamReader for easy access.  
                StreamReader reader = new StreamReader(dataStream);
                // Read the content.  
                responseFromServer = reader.ReadToEnd();
                // Display the content.  
                Console.WriteLine(responseFromServer);
            }
            // Close the response.  
            response.Close();
            return responseFromServer;
        }
    }
}
