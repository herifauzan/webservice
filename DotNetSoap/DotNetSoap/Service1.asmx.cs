﻿using System;
using System.Web;
using System.Web.Services;

namespace DotNetSoap
{
    [WebService]
    public class Service1 : System.Web.Services.WebService
    {
        [WebMethod]
        public string firstMethod(string input)
        {
            return input + " : " + DateTime.Now.ToString() + " Input was changed";
        }
    }
}
