//inisiasi
var express = require('express');
var bodyParser = require('body-parser');
var jwt = require('jsonwebtoken');
var cors  = require('cors');

var app = express();
var router = express.Router();
var port = process.env.PORT || 3003;

//settingan database
var config = require('./config');
var mongoose = require('mongoose');
mongoose.connect(config.database, { useNewUrlParser: true });
var User = require('./models/user');


//konfigurasi body parser
app.use(bodyParser.urlencoded( {extended: true}));
app.use(bodyParser.json());
//jwt
app.set('secretKey', config.secret);
app.use(cors());
//===DAFTAR ENDPOINT
// /api/users       :GET :POST
// /api/users/:name     :GET :PUT :DELETE

//using middleware to logging
app.use(function (req, res, next) {
    console.log('Using the api at Time:', Date.now())
    next()
})

//login
router.post('/login', function(req, res){
    User.findOne({
      username: req.body.username
    }, function(err, user){
      if(err) throw err;
      //console.log(req.body)
      if(!user){
        res.json({ success: false, message: 'User tidak terdaftar' });
      }else {
        //harusnya passwordnya hash
        if (user.password != req.body.password) {
          res.json({ success: false, message: 'password user salah!' });
        }else {
          //membuat token
          var token = jwt.sign(user.toJSON(), app.get('secretKey'), {
            expiresIn: "2 days"
          });
          //check token yg dikirim
          //console.log("mengirim jwt token: "+token)
          //ngirim balik token
          res.json({
            success : true,
            message: 'token berhasil didapatkan!',
            token  : token
          })
        }
      }
    });
  });

//test router
router.get('/', function(req, res){
    res.json({ message: 'Welcome!!!'});
})

//proteksi route dengan token
router.use(function(req, res, next){
    //mengambil token: req.body.token || req.query.token ||
    var token = req.headers['authorization'];
  
    //decode token
    if(token){
  
      jwt.verify(token, app.get('secretKey'), function(err, decoded){
        if(err)
          return res.json({ success: false, message: 'problem dengan token' });
        else {
          req.decoded = decoded;
  
          //apakah sudah expire
          if(decoded.exp <= Date.now()/1000) {
            return res.status(400).send({
              success:false,
              message:'token sudah expired',
              date   : Date.now()/1000,
              exp    : decoded.exp
            });
          }
  
          next();
        }
      });
  
    }else{
      return res.status(403).send({
        success:false,
        message:'token tidak tersedia'
      });
    }  
});

router.route('/users')
    .post(function(req, res){
        var user = new User();
        user.fullname = req.body.fullname;
        user.username = req.body.username;
        user.email = req.body.email;
        user.password = req.body.password;
        user.address = req.body.address;
        user.postalcode = req.body.postalcode;
        user.phonenumber= req.body.phonenumber;
        user.role = req.body.role;
        user.save(function(err){
            if(err) res.send(err);
            res.json({message: "User successfully added"});
        })
    })
    .get(function(req, res){
        User.find(function(err, users){
            if(err) res.send(err);
            res.json(users);
        })
    });

router.route('/users/:username')
    .get(function(req, res){
        User.find({username: req.params.username}, function(err, user){
            if(err) res.send(err);
            res.json(user);
        })
    }).put(function(req, res){
        var user= User.find({username: req.params.username}, function(err, user){
            if(err) res.sendStatus(404);
        })
        if(req.params.username == null || user == null) res.sendStatus(404);

        if(req.body.username == null) req.body.username = user.username;
        if(req.body.password == null) req.body.password = user.password;
        if(req.body.email == null) req.body.email = user.email;
        if(req.body.fullname == null) req.body.fullname = user.fullname;
        if(req.body.address == null) req.body.address = user.address;
        if(req.body.postalcode == null) req.body.postalcode = user.postalcode;
        if(req.body.phonenumber == null) req.body.phonenumber = user.phonenumber;
        if(req.body.role == null) req.body.role = user.role;
        User.update(
          { username:req.params.username },
          { username:req.body.username,  
            password: req.body.password,
            email: req.body.email,
            fullname: req.body.fullname,
            address: req.body.address,
            postalcode: req.body.postalcode,
            phonenumber: req.body.phonenumber,
            role: req.body.role
          },
          function(err, user) {
            if(err) res.sendStatus(404);
            res.json(" user berhasil diupdate! ");
        });
    }).delete(function(req, res) {
      User.remove({
        username: req.params.username
      }, function(err, user){
        if(err) res.send(err);
        res.json({message: "user berhasil dihapus!"});
      });
    });


//prefix API
app.use('/api', router);

//running in port
app.listen(port);
console.log('Server is running on port'+port);