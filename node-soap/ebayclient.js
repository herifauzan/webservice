var soap = require('soap');
//var urlRead = 'https://developer.ebay.com/webservices/finding/latest/FindingService.wsdl';
url = 'https://developer.ebay.com/webservices/finding/latest/FindingService.wsdl';
// Create client
soap.createClient(url, function (err, client) {
  if (err){
    throw err;
  }
   
  /* 
  * Parameters of the service call: they need to be called as specified
  * in the WSDL file
  */
var headers={
 	     'X-EBAY-SOA-OPERATION-NAME':'findItemsByKeywords',
             'X-EBAY-SOA-SECURITY-APPNAME': 'HeriFauz-SimpleLi-PRD-ed8d33955-e0a56611',
	     'X-EBAY-API-COMPATIBILITY-LEVEL':'967',
	     'X-EBAY-API-CALL-NAME': 'findItemsByKeywords',
	     'X-EBAY-API-SITEID':'216'}
  client.addSoapHeader(headers);
  // call the service
  client.findItemsByKeywords('harry potter', function (err, res) {
    if (err) console.log(err);
      // print the service returned result
     console.log(res);
  });
 
  
});
