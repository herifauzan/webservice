/*jslint node: true */
"use strict";


var soap = require('soap');
var express = require('express');
var fs = require('fs');
//connect to database mysql
var mysql = require('mysql');
var con = mysql.createConnection({
  host: "localhost",
  user: "heri",
  password: "admin"
});

con.connect(function(err) {
  if (err) throw err;
  console.log("Connected!");
});

//tambahin function asal name,description, price, photo
function addProduct(args){
    console.log(args);
    return{
        result: "bisa dibaca"
    }
}
function readAll(args){
    console.log(args);
    return{
        result: "bisa dibaca"
    }
}
function readOne(args){
    console.log(args);
    return{
        result: "bisa dibaca"
    }
}
function updateOne(args){
    console.log(args);
    return{
        result: "bisa dibaca"
    }
}
function deleteOne(args){
    console.log(args);
    return{
        result: "bisa dibaca"
    }
}
// the service
var serviceObject = {
  product_provider: {
        product_provider_add: {
            addproduct: addProduct
        },
        product_provider_readall: {
            readall: readAll
        },
        product_provider_read: {
            read: readOne
        },
        product_provider_update: {
            update: updateOne
        },
        product_provider_delete: {
            delete: deleteOne
        }
    }
};

// load the WSDL file
var xml = fs.readFileSync('services.wsdl', 'utf8');
// create express app
var app = express();

// root handler
app.get('/', function (req, res) {
  res.send('Node Soap Running!');
})

// Launch the server and listen
var port = 8000;
app.listen(port, function () {
  console.log('Listening on port ' + port);
  var wsdl_path = "/wsdl";
  soap.listen(app, wsdl_path, serviceObject, xml);
  console.log("Check http://localhost:" + port + wsdl_path +"?wsdl to see if the service is working");
});