var Ebay = require('ebay')
var express = require('express');

var app = express();
var router = express.Router();
var port = process.env.PORT || 3005;

function findingQuery(input){
  var params = {
    'OPERATION-NAME': 'findItemsByKeywords'
  , 'keywords': input
  }
  var ebay = new Ebay({
    app_id: 'HeriFauz-SimpleLi-PRD-ed8d33955-e0a56611'
  })
  
  var result = ebay.get('finding', params, function (err, data) {
    console.log(JSON.stringify(params))
    if(err) throw err
    //if(params.keywords == "") return JSON.stringify([])
    //id = data['findItemsByKeywordsResponse'][0]['searchResult'][0]['item'][0]['itemId'][0]
    //name = data['findItemsByKeywordsResponse'][0]['searchResult'][0]['item'][0]['title'][0]
    //description = data['findItemsByKeywordsResponse'][0]['searchResult'][0]['item'][0]['subtitle'][0]
    //price = data['findItemsByKeywordsResponse'][0]['searchResult'][0]['item'][0]['sellingStatus'][0]['currentPrice'][0]['__value__']
    //url = data['findItemsByKeywordsResponse'][0]['searchResult'][0]['item'][0]['viewItemURL'][0]
    //console.log(JSON.stringify(data))
    product = []
    list = data['findItemsByKeywordsResponse'][0]['searchResult'][0]['item']
    for (const itemId in list) {
      if (list.hasOwnProperty(itemId)) {
        const element = list[itemId];
        try{
          var newStructure = { 
            id: element['itemId'][0], 
            name: element['title'][0],
            description: element['subtitle'][0],
            price: element['sellingStatus'][0]['currentPrice'][0]['__value__'],
            url: element['viewItemURL'][0]
          }
          product.push(newStructure);
        }catch(err){

        }
        return product;
      }
    } 
  })
  console.log(JSON.stringify(result));
  return result;
}


router.get('/get/:keyword', function(req, res){
  console.log(req.params)
  var key = req.params.keyword;
  try{
    //res.json(findingQuery(key));
    var params = {
      'OPERATION-NAME': 'findItemsByKeywords'
    , 'keywords': key
    }
    var ebay = new Ebay({
      app_id: 'HeriFauz-SimpleLi-PRD-ed8d33955-e0a56611'
    })
    ebay.get('finding', params, function (err, data) {
      console.log(JSON.stringify(params))
      if(err) throw err
      //if(params.keywords == "") return JSON.stringify([])
      //id = data['findItemsByKeywordsResponse'][0]['searchResult'][0]['item'][0]['itemId'][0]
      //name = data['findItemsByKeywordsResponse'][0]['searchResult'][0]['item'][0]['title'][0]
      //description = data['findItemsByKeywordsResponse'][0]['searchResult'][0]['item'][0]['subtitle'][0]
      //price = data['findItemsByKeywordsResponse'][0]['searchResult'][0]['item'][0]['sellingStatus'][0]['currentPrice'][0]['__value__']
      //url = data['findItemsByKeywordsResponse'][0]['searchResult'][0]['item'][0]['viewItemURL'][0]
      //console.log(JSON.stringify(data))
      product = []
      list = data['findItemsByKeywordsResponse'][0]['searchResult'][0]['item']
      for (const itemId in list) {
        if (list.hasOwnProperty(itemId)) {
          const element = list[itemId];
          try{
            var newStructure = { 
              id: element['itemId'][0], 
              name: element['title'][0],
              description: element['subtitle'][0],
              price: element['sellingStatus'][0]['currentPrice'][0]['__value__'],
              url: element['viewItemURL'][0]
            }
            product.push(newStructure);
          }catch(err){
  
          }
        }
      } 
      return res.json(product);
    })
  }catch(err){
    res.json({ message: 'Error!!!'});
  }
})
app.use('/api', router);
app.listen(port);
console.log('Server is running on port'+port);