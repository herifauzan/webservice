using Microsoft.EntityFrameworkCore;

namespace DotNetRest.Models
{
    public class ApiContext: DbContext
    {
        public ApiContext(DbContextOptions<ApiContext> options)
            : base(options)
        {}

        public DbSet<Product> product { get; set; }
    }
}