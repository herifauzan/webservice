using System;
using System.ComponentModel.DataAnnotations;

namespace DotNetRest.Models
{
    public class Product
    {
        public int id {get; set;}

        public DateTime create_date {get; set;}
        public string name {get; set;}
        public string description {get; set;}
        public long price {get; set;}
        public string photo {get; set;}
        public int seller_id {get; set;}
    }
}