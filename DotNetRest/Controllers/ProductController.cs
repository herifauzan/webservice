using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DotNetRest.Models;

namespace DotNetRest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController: ControllerBase
    {
        private ApiContext _context; 
        public ProductController(ApiContext context)
        {
            _context = context;

            if (_context.product.Count() == 0)
            {
              //send empty products
            }
        }
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<Product>> Get()
        {
            return _context.product.ToList();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<Product> Get(int id)
        {
            Product result= _context.product.Find(id);
            return result;
        }
       

        // POST api/values
        [HttpPost]
        public ActionResult<IEnumerable<string>> Post([FromBody] Product value)
        {
            if (_context.product.Where(x=> x.id == value.id).FirstOrDefault() == null)
            {
                _context.product.Add(new Product { name= value.name, create_date= DateTime.Now,description=value.description, price=value.price, photo= value.photo, seller_id= value.seller_id });
                _context.SaveChanges();
                return new string[]{"Successfully Creating Product "};
            }
            return new string[]{"Failed Creating Product"};
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public ActionResult<IEnumerable<string>> Put(int id, [FromBody] Product value)
        {
            Product result= _context.product.Find(id);
            if(result != null){
                result.name = value.name;
                result.description = value.description;
                result.price = value.price;
                result.photo = value.photo;
                //result.update(new Product { name= value.name, create_date= DateTime.Now,description=value.description, price=value.price, photo= value.photo, seller_id= value.seller_id });
                _context.SaveChanges();
                return new string[]{"Succesfully updating id "+id};
            }
            return new string[]{"Failed to find id "+id};

        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public ActionResult<IEnumerable<string>> Delete(int id)
        {
            var check = _context.product.Where(x=> x.id.Equals(id)).FirstOrDefault();
            if(check != null) {
                _context.Remove(_context.product.Where(x=> x.id.Equals(id)).FirstOrDefault());
                _context.Remove(check);
                _context.SaveChanges();
                return new string[]{"Succesfully Deleting Product "+id};
            }
            return new string[]{"Failed to Delete Product "+id};
        }
    }
}
